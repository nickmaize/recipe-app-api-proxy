# Contains instructions to build our image
# Best Practice - Docker service running as a least-privilege user.
FROM nginxinc/nginx-unprivileged:1-alpine
LABEL maintainer="nickmaize@gmail.com"

# Copy files into container
# By default nginx expects config files inside of /etc/nginx
COPY ./default.conf.tpl /etc/nginx/default.conf.tpl
COPY ./uwsgi_params /etc/nginx/uwsgi_params

# Define Default Environment Variables
ENV LISTEN_PORT=8000
# ENV APP_HOST=app
ENV APP_PORT=9000

# Switch to root user for required permissions to make changes
USER root
# Create new Directory (-p will create subdirectories if it doesn't exist)
RUN mkdir -p /vol/static
# Set permissions to directory
RUN chmod 755 /vol/static
# Create default.conf file
RUN touch /etc/nginx/conf.d/default.conf
# Change Ownership to user:group, which is running the entrypoint script
RUN chown nginx:nginx /etc/nginx/conf.d/default.conf

# Copy file into container
COPY ./entrypoint.sh /entrypoint.sh
# Set file to be executable
RUN chmod +x /entrypoint.sh

# Switch back to nginx user
USER nginx

# Define command to run Dockerfile
CMD ["/entrypoint.sh"]



