#!/bin/sh

# During Execution, if any line fails, return failure w/error. Helps with debugging
set -e

# Pass environment variables using envsubst
# Move < template file to substitue > output config (Destination path is default location for nginx config)
# envsubst < /etc/nginx/default.conf.tpl > /etc/nginx/conf.d/default.conf

# Start nginx service. Command enables nginx to run in foreground of Docker execution.
# This all logs from nginx server are printed to the Docker Output
nginx -g 'daemon off;'

